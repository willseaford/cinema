package cinema.entities;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Film")
public class Film {
	
	@Id
	@Column(name = "FILM_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@OneToOne
	@Column(name = "FILM_GENRE", nullable = false)
	private String genre;
	
	@OneToOne
	@Column(name = "FILM_LENGTH", nullable = false)
	private String length;
	
	@OneToOne
	@Column(name = "FILM_TITLE", nullable = false)
	private String title;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DATE", nullable = false)
	@NotNull
	private ArrayList<Date> date;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public ArrayList<Date> getDate() {
		return date;
	}

	public void setDate(ArrayList<Date> date) {
		this.date = date;
	}

	

}
