package cinema.entities;

import java.awt.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table (name = "customers")
public class Customer {
	
	@Id
	@Column (name  = "cid")
	@GeneratedValue ( strategy = GenerationType.IDENTITY)
	private Long id;
	@Column (name = "firstname", nullable = false, length = 225)
	@NotNull
	@Size (min = 2, max = 225)
	private String firstName;
	@Column (name = "familyname", nullable = false, length = 225)
	@NotNull
	@Size (min = 2, max = 225)
	private String familyName;
	
	@JoinColumn(name="address_fk", nullable = false)
	@OneToOne
	private Address address;
	
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	

}
