package cinema.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Address")
public class Address {

	@Id
	@Column(name = "LINE1", length = 255, nullable = false)
	@NotNull
	private String line1;

	@Column(name = "LINE2", length = 255)
	private String line2;

	@Id
	@Column(name = "POSTCODE", length = 9, nullable = false)
	@NotNull
	private String postcode;
	
	@Id
	@Column(name = "CITY", length = 255, nullable = false)
	@NotNull
	private String city;
	
	
	@Id
	@Column(name = "COUNTRY", length = 255, nullable = false)
	@NotNull
	private String country;
	
	public Address(String line1, String postcode){
		this.line1=line1;
		this.postcode = postcode;
	}

	public Address(String line1, String line2, String postcode, String city, String country ) {
		this.line1 = line1;
		this.line2 = line2;;
		this.postcode = postcode;
		this.city = city;
		this.country=country;
		
	}
	
	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}


}
